def ReverseInput(text) -> object:
    """


    :rtype : object
    :param text:
    :return:
    """
    return text.reverse()

print(ReverseInput(input()))
