import math


# def gen_num():
#     i = 0
#     n = 1
#     while 1:
#         i += 1
#         n = (i * (3*i - 1)) / 2
#         yield n


def is_pentagonal(n):
    if n < 0:
        return False
    value = (math.sqrt(1 + 24 * n) + 1) / 6.0
    return value == int(value)


# pentagon_number_gen = gen_num()
index = 1
found = True
result = 0
while found:
    index += 1
    n = (index * (3 * index - 1)) / 2
    for j in range(index - 1, 0, -1):
        m = (j * (3 * j - 1)) / 2
        if is_pentagonal(n - m) and is_pentagonal(n+m):
            found = False
            result = n - m

print(result)