"""
# 006_sum_square_different.py
# Author: coozplz
# Date: 2014. 08. 06
# Problem: 1~100까지 합의 제곱과 제곱의 차를 구하시오.
"""


def sum_of_squares(s_index, e_index):
    """
    제곱의 합을 구한다.
    :param s_index: 시작 번호
    :param e_index: 종료 번호
    :return: 제곱의 합
    """
    sum_result = 0
    for i in range(s_index, e_index+1, 1):
        sum_result += (i * i)
    return sum_result


def square_of_sum(s_index, e_index):
    """
    합의 제곱을 구한다.
    :param s_index: 시작 번호
    :param e_index: 종료 번호
    :return: 합의 제곱
    """
    sum_result = 0
    for i in range(s_index, e_index+1, 1):
        sum_result += i
    return sum_result * sum_result


result1 = sum_of_squares(1, 100)
result2 = square_of_sum(1, 100)

# 제곱의 차를 구한다.
print(result2 - result1)
