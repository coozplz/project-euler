def prime_factor(number):
    """
    # PrimeFactor를 배열 형태로 반환한다.
    :param number:
    :return:
    """
    n = number
    factors = []
    i = 2
    while i <= int(n / i):
        while n % i == 0:
            factors.append(i)
            n /= i
        i += 1
    if n > 1:
        factors.append(int(n))
    return factors


result = prime_factor(600851475143)
maxValue = 0
for prime in result:
    maxValue = max(maxValue, prime)
print(maxValue)