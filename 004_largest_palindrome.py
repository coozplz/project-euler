"""
# 004_largest_palindrome.py
# Author: coozplz@gmail.com
# Date: 2014. 08. 07
# Problem:
    A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.

    Find the largest palindrome made from the product of two 3-digit numbers.
"""


def is_palindrome(num):
    """
    숫자가 회문인지 확인한다.
    검증1. 회문의 문자 길이는 짝수이다.
    검증2. 회문의 문자 길의 절반까지의 값은 나머지 절반의 값의 역순과 같다.
    :param num: 숫자
    :return: 회문 여부
    """
    str_num = str(num)
    if len(str_num) % 2 != 0:
        return False
    start_num = str_num[:int(len(str_num) / 2)]
    end_num = str_num[int(len(str_num) / 2):]

    if start_num == "".join(reversed(end_num)):
        return True
    return False

max_palindrome = 0
for i in range(999, 1, -1):
    for j in range(999, 1, -1):
        multiple = i * j
        if is_palindrome(multiple):
            max_palindrome = max(max_palindrome, multiple)

print(max_palindrome)