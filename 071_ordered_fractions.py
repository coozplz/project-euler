import time



def gcd(x, y):
    if x < y:
        x, y = y, x
    while y > 0:
        x, y = y, (x % y)
    return x


def ordered_fractions(num):
    max_value = 0
    found_value = ()
    for i in range(2, num + 1):
        for j in range(1, i):
            mod = j / i
            if mod > (3/7) or gcd(j, i) != 1:
                continue
            elif mod < (3/7):
                if mod > max_value:
                    max_value = mod
                    found_value = (j, i, mod)
    print(found_value)



if __name__ == '__main__':
    s_time = time.clock()
    ordered_fractions(1000000)

    print("It takes %s", time.clock() - s_time)