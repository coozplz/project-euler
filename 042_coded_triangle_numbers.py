

def get_triangle_nums(max_value):
	index = 1
	triangle_nums = []
	while True:
		triangle_num = (index*index + index) / 2
		triangle_nums.append(triangle_num)
		if triangle_num >= max_value:
			break
		index += 1
	return triangle_nums





triangle_nums = get_triangle_nums(200)

file = open("/Users/coozplz/Downloads/p042_words.txt")
words = file.readline().split(',')

triangle_word_count = 0
for word in words:
	substrWord = word[1:len(word)-1]
	sum_value = 0	
	for ch in substrWord:
		index = ord(ch) - ord('@')
		sum_value += index

	if sum_value in triangle_nums:
		triangle_word_count += 1
	
print(triangle_word_count)


