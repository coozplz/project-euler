"""
# 007_10001st_prime.py
# Date: 2014. 07. 22
# Author: coozplz@gmail.com
"""
import math


def is_prime(n):
    """
    소수를 판단한다.
    :param n:
    :return:
    """
    result = True
    for i in range(2, int(math.sqrt(n)) + 1):
        if n % i == 0:
            result = False
            break

    return result

count = 10001
i = 2
while 1:
    if is_prime(i):
        count -= 1
    if count == 0:
        break
    i += 1
print("Number is =", i)