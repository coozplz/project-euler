"""
# 19_Amicable_Numbers.py
# Date: 2014. 07. 31
# Author: coozplz@gmail.com
"""
num = 10000
sumOfAmicable = 0

while num > 0 :
    sumOfDivisor = 0
    for i in range(1, num):
        if num % i == 0:
            # 약수의 합을 구한다.
            sumOfDivisor += i

    sumOfDivisor2 = 0
    for i in range(1, sumOfDivisor):
        if sumOfDivisor % i == 0:
            # 약수의 합을 구한다.
            sumOfDivisor2 += i


    """
    # 약수의 합과 수가 일치하지만 두수가 동일한 경우는 제외한다.
    # 예) 28 // 28
    #      6 // 6
    """
    if sumOfDivisor2 == num and num != sumOfDivisor :
        print("AmicableNumber=", num, sumOfDivisor)
        sumOfAmicable += num
    num -=  1
print(sumOfAmicable)