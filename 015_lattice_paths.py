ROUTE = 20
def factorial(n):
    sum_value = 1
    for i in range(n, 0, -1):
        sum_value *= i
    return sum_value


if __name__ == '__main__':
    print(factorial(ROUTE+ROUTE) / (factorial(ROUTE) * factorial(ROUTE)))