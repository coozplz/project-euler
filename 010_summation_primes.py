import math
import time


def is_prime(n):
    result = True
    for i in range(2, int(math.sqrt(n)) + 1):
        if n % i == 0:
            result = False
            break
     
    return result


var = 2000000
sTime = time.clock()


sum_result = 0
for i in range(2, var-1):
    if is_prime(i):
        sum_result += i
eTime = time.clock()
print("input = ", var, " sum =", sum_result, "time=", (eTime-sTime) * 100, "ms")
