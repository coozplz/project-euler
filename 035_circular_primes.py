import math
import time


def is_prime(n):
    if n < 2:
        return False

    if n == 2:
        return True

    if n % 2 == 0:
        return False

    for i in range(3, int(math.sqrt(n)) + 1, 2):
        if n % i == 0:
            return False
    return True


if __name__ == '__main__':
    s_time = time.clock()
    count = 0
    for i in range(1, (10 ** 6) + 1):
        s_i = list(str(i))

        if is_prime(i):
            found = True
            for index in range(1, len(s_i)):
                value = str(s_i.pop())
                s_i.insert(0, value)
                if not is_prime(int(''.join(s_i))):
                    found = False
                    break
            if found:
                count += 1
    print(count)
