import time


def getCollatzChain(num):
    arr_result = [num]
    while True:
        if num % 2 == 0:
            num //= 2
        else:
            num = (num*3) + 1
        if num == 1:
            arr_result.append(1)
            break
        arr_result.append(num)
    return arr_result


def collatz(n):
    return n // 2 if n%2 == 0 else 3*n + 1

def distance(n, cache={1:1}):
    if n not in cache: cache[n] = distance(collatz(n)) + 1
    return cache[n]


if __name__ == '__main__':
    sTime = time.clock()
    print(max(range(1,1000000), key=distance), time.clock() - sTime)
    sTime = time.clock()
    max_value = 0
    foundIndex = 0
    for i in range(1, 1000000):
        arr_result = getCollatzChain(i)
        if len(arr_result) > max_value:
            max_value = len(arr_result)
            foundIndex = i
            print(max_value, i)
    print(foundIndex, max_value, time.clock() - sTime)


