def gcd(a, b):
   m = max(a,b)
   n = min(a,b)
   while n != 0:
       t = m%n
       (m,n)=(n,t)
   return abs(m)
