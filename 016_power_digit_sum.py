import functools
import time


sTime = time.clock()
sumOfPowerDigit = functools.reduce(lambda x, y: int(x) + int(y), str(2 ** 1000))
print(sumOfPowerDigit, time.clock() - sTime)
