"""
# 048_self_powers.py
# Date: 2014. 08. 16
# Author: coozplz@gmail.com
# 요약:
      단순 반복으로 1^1 + 2^2 + 3^3 + 4^4 ... 1000^1000 을 구한다.
"""
sum_value = 0
for i in range(1, 1000 + 1):
    temp_sum = i
    for j in range(1, i):
        temp_sum *= i
    sum_value += temp_sum

# 마지막 10자리를 자른다.
print(str(sum_value)[len(str(sum_value)) - 10: ])