"""
# 012_highly_divisible_triangular_number.py
# Date: 2014. 08. 12
# Author: coozplz@gmail.com
# 풀이방법
    FOR 1 to N
        A = 1 + ... + N
        IF A 의 약수의 수가 500개 이상
        THEN BREAK
    PRINT(A)
"""
import time
import math


def get_divisor_count(num):
    """
    약수의 개수를 구한다.
    약수개수 구하기는 소인수분해 후 지수의 곱셈으로 한다.
    :param num: 숫자
    :return: 약수의 개수
    """
    primes = []
    p = 2

    #소인수분해를 하고 결과를 list에 저장한다.
    while num != 1:
        if num % p == 0:
            num /= p
            primes.append(p)
            p = 2
        else:
            p += 1

    # list 에서 중복되는 값을 제거하기 위해 set에 저장한다.
    s = set([x for x in primes])
    divisor_count = 1
    for i in s:
        # 약수의 개수 구하기 공식
        # [2,2,3,11] => (2의 개수+1) * (3의 개수 +1 ) * (11의 개수 +1)
        # 결과 => 12개
        divisor_count *= (primes.count(i) + 1)
    return divisor_count


sTime = time.clock()
index = 1
while True:
    sumResult = index * (index+1) // 2
    count = get_divisor_count(sumResult)
    if count >= 500:
        print(sumResult, time.clock() - sTime)
        break

    # if count > 100:
    #     # 프로그램이 정상적으로 돌고 있는지 확인하기 위한 로그
    #     print(sumResult, count)
    # if count >= 500:
    #     # 원하는 결과인 약수의 개수가 500개 이상이 되면 프로그램을 종료한다.
    #     break
    index += 1
    # print(endIndex, count, sumResult)


