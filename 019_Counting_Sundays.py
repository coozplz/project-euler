from datetime import *

sum_of_sundays = 0
year = 1901
month = 1

for year in range(1901, 2001):
    for month in range(1, 13):
        curr_day = date(year, month, 1)
        if curr_day.weekday() == 6:
            sum_of_sundays += 1

print(sum_of_sundays)
