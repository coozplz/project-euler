"""
# 2_EventFibonacchiNumbers.py
# Author: coozplz@gmail.com
# Date: 2014. 08. 06
# Question: 1,2로 시작하는 피보나치 수열을 4,000,000이 넘기 전까지의 짝수의 합
"""
LIMIT_VALUE = 4000000


def fibo_recursive(n):
    """
    피보나치 수열을 재귀적으로 구현
    :param n: 숫자
    :return:
    """
    if n < 2:
        return n
    else:
        return fibo_recursive(n-1) +fibo_recursive(n-2)


i = 2
sum_result = 0
while True:
    fibo_result = fibo_recursive(i)
    if fibo_result % 2 == 0:
        sum_result += fibo_result
    i += 1
    if fibo_result > LIMIT_VALUE:
        break
print(sum_result)