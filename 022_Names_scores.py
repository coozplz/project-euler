"""
# 22_Names_scores.py
# Date: 2014. 08. 01
# Author: coozplz@gmail.com
# 학습내용
    1. 파일 읽기
    2. 리스트 정렬
    3. Character 를 ASCII 코드값으로 출력
"""

#이름 정보를 저장할 파일명
nameList = []

f = open("./resource/names.txt", "r")
# 파일을 읽는다.
line = f.read()

# 라인을 , 를 구분으로 자른다.
aStr = line.split(",")

for n in aStr:
    nameList.append(n)

print("Size =", len(nameList))

#이름을 정렬한다.
nameList.sort()

# 합산 점수를 저장
sumOfScores = 0

# 이름의 위치한 행의 번호
rowCount = 1


for name in nameList:
    nameCount = 0
    '''
    # 이름에 따옴표가 포함된 경우 제거
    '''
    name = name.replace("\"", "")
    for ch in name:
        '''
        # 이름을 한자씩 잘라서 ASCII 값으로 알파벳의 덧셈을 처리한다.
        # Ex) A = 1, B = 2
        '''
        nameCount += (ord(ch) - (ord('A')-1))

    sumOfScores += (nameCount * rowCount)
    rowCount += 1
print(sumOfScores)









